# Paper3D Snippets

Paper3D Snippets is a collection of various pieces of source code from the Paper3D game project.

The source is provided as examples of functionality found within the game.

The repo is also a landing point from my website for interested people.

Further information regarding Paper3D can be found at [Aura Forge Productions](https://auraforge.ca)