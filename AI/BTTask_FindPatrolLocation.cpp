// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_FindPatrolLocation.h"
#include "Engine/TargetPoint.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "GuardAIController.h"
#include "NavigationSystem.h"

EBTNodeResult::Type UBTTask_FindPatrolLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    AGuardAIController* AIController = Cast<AGuardAIController>(OwnerComp.GetAIOwner());
    if(AIController == nullptr)
    {
        return EBTNodeResult::Failed;
    }

    ATargetPoint* Waypoint = AIController->GetWaypoint();
    if(Waypoint)
    {
        const float SearchRadius = 200.0f;
        const FVector SearchOrigin = Waypoint->GetActorLocation();

        FNavLocation ResultLocation;
        UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetNavigationSystem(AIController);
        if(NavSystem && NavSystem->GetRandomPointInNavigableRadius(SearchOrigin, SearchRadius, ResultLocation))
        {
            // NOTE : The selected key should be "PatrolLocation" in the BehaviorTree setup
            OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(BlackboardKey.GetSelectedKeyID(),
                                                                                    ResultLocation.Location);
            return EBTNodeResult::Succeeded;
        }
    }

    return EBTNodeResult::Failed;
}
