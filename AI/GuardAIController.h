// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GuardAIController.generated.h"

class ATargetPoint;
class APaperCharacter;

/**
 *
 */
UCLASS()
class SHADOWSITE_API AGuardAIController : public AAIController
{
    GENERATED_BODY()

    virtual void Possess(class APawn* InPawn) override;

    class UBehaviorTreeComponent* BehaviorComponent;
    class UBlackboardComponent* BlackboardComponent;

    UPROPERTY(EditDefaultsOnly, Category = "AI")
    FName TargetEnemyKeyName;

    UPROPERTY(EditDefaultsOnly, Category = "AI")
    FName TargetLocationKeyName;

    UPROPERTY(EditDefaultsOnly, Category = "AI")
    FName PatrolLocationKeyName;

    UPROPERTY(EditDefaultsOnly, Category = "AI")
    FName CurrentWaypointKeyName;

    UPROPERTY(EditDefaultsOnly, Category = "AI")
    FName BehaviorTypeKeyName;

public:
    AGuardAIController(const FObjectInitializer& ObjectInitializer);

    ATargetPoint* GetWaypoint();
    APaperCharacter* GetTargetEnemy();
    void SetMoveToTarget(APawn* Pawn);
    void SetWaypoint(ATargetPoint* NewWaypoint);
    void SetTargetEnemy(APawn* NewTarget);

    FORCEINLINE UBehaviorTreeComponent* GetBehaviorComponent() const
    {
        return BehaviorComponent;
    }

    FORCEINLINE UBlackboardComponent* GetBlackboardComponent() const
    {
        return BlackboardComponent;
    }

};
