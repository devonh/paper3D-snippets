// Fill out your copyright notice in the Description page of Project Settings.


#include "GuardAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Engine/TargetPoint.h"
#include "PaperGuard.h"

AGuardAIController::AGuardAIController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    BehaviorComponent = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComponent"));
    BlackboardComponent = ObjectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackboardComponent"));

    // NOTE : Match with the AI Blackboard
    TargetEnemyKeyName = "TargetEnemy";
    TargetLocationKeyName = "TargetLocation";
    PatrolLocationKeyName = "PatrolLocation";
    CurrentWaypointKeyName = "CurrentWaypoint";
    BehaviorTypeKeyName = "BehaviorType";
}

void AGuardAIController::Possess(APawn* InPawn)
{
    Super::Possess(InPawn);

    APaperGuard* Guard = Cast<APaperGuard>(InPawn);
    if(!Guard)
    {
        return;
    }

    if(Guard->BehaviorTree->BlackboardAsset)
    {
        BlackboardComponent->InitializeBlackboard(*Guard->BehaviorTree->BlackboardAsset);
        BlackboardComponent->SetValueAsEnum(BehaviorTypeKeyName, static_cast<uint8>(Guard->BehaviorType));
    }

    BehaviorComponent->StartTree(*Guard->BehaviorTree);
}

ATargetPoint* AGuardAIController::GetWaypoint()
{
    if(BlackboardComponent)
    {
        return Cast<ATargetPoint>(BlackboardComponent->GetValueAsObject(CurrentWaypointKeyName));
    }

    return nullptr;
}

APaperCharacter* AGuardAIController::GetTargetEnemy()
{
    if(BlackboardComponent)
    {
        return Cast<APaperCharacter>(BlackboardComponent->GetValueAsObject(TargetEnemyKeyName));
    }

    return nullptr;
}

void AGuardAIController::SetMoveToTarget(APawn* Pawn)
{
    if(BlackboardComponent)
    {
        SetTargetEnemy(Pawn);

        if(Pawn)
        {
            BlackboardComponent->SetValueAsVector(TargetLocationKeyName, Pawn->GetActorLocation());
        }
    }
}

void AGuardAIController::SetWaypoint(ATargetPoint* NewWaypoint)
{
    if(BlackboardComponent)
    {
        BlackboardComponent->SetValueAsObject(CurrentWaypointKeyName, NewWaypoint);
    }
}

void AGuardAIController::SetTargetEnemy(APawn* NewTarget)
{
    if(BlackboardComponent)
    {
        BlackboardComponent->SetValueAsObject(TargetEnemyKeyName, NewTarget);
    }
}
