// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_FindWaypoint.h"
#include "Engine/TargetPoint.h"
#include "GuardAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "Kismet/GameplayStatics.h"

EBTNodeResult::Type UBTTask_FindWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    AGuardAIController* MyController = Cast<AGuardAIController>(OwnerComp.GetAIOwner());
    if (MyController == nullptr)
    {
        return EBTNodeResult::Failed;
    }

    AActor* NewWaypoint = nullptr;

    // Iterate all the bot waypoints in the current level and find a new random waypoint to set as destination
    TArray<AActor*> AllWaypoints;
    UGameplayStatics::GetAllActorsOfClass(MyController, ATargetPoint::StaticClass(), AllWaypoints);

    if (AllWaypoints.Num() == 0)
        return EBTNodeResult::Failed;

    // Find a new waypoint randomly by index (this can include the current waypoint)
    // For more complex or human AI you could add some weights based on distance and other environmental conditions here
    NewWaypoint = AllWaypoints[FMath::RandRange(0, AllWaypoints.Num() - 1)];

    // Assign the new waypoint to the Blackboard
    if (NewWaypoint)
    {
        // NOTE : The selected key should be "CurrentWaypoint" in the BehaviorTree setup
        OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(BlackboardKey.GetSelectedKeyID(),
                                                                                NewWaypoint);
        return EBTNodeResult::Succeeded;
    }

    return EBTNodeResult::Failed;
}
