float4 luminance = float4(0.30, 0.59, 0.11, 1);

// TOP ROW
float s11 = dot(Texture2DSample(Tex, TexSampler, UV + float2(-1.0f / texW, -1.0f / texH)), luminance);
float s12 = dot(Texture2DSample(Tex, TexSampler, UV + float2(0, -1.0f / texH)), luminance);
float s13 = dot(Texture2DSample(Tex, TexSampler, UV + float2(1.0f / texW, -1.0f / texH)), luminance);

// MIDDLE ROW
float s21 = dot(Texture2DSample(Tex, TexSampler, UV + float2(-1.0f / texW, 0)), luminance);
float s23 = dot(Texture2DSample(Tex, TexSampler, UV + float2(1.0f / texW, 0)), luminance);

// LAST ROW
float s31 = dot(Texture2DSample(Tex, TexSampler, UV + float2(-1.0f / texW, 1.0f / texH)), luminance);
float s32 = dot(Texture2DSample(Tex, TexSampler, UV + float2(0, 1.0f / texH)), luminance);
float s33 = dot(Texture2DSample(Tex, TexSampler, UV + float2(1.0f / texW, 1.0f / texH)), luminance);

float sobel_h = s11 + (2 * s12) + s13 - s31 - (2 * s32) - s33;
float sobel_v = s11 + (2 * s21) + s31 - s13 - (2 * s23) - s33;

float4 result;

if (((sobel_h * sobel_h) + (sobel_v * sobel_v)) > 0.05)
{
	result = float4(0,0,0,1);
}
else
{
	result = float4(1,1,1,1);
}

return result;
