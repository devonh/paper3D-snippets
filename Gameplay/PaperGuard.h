// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "PaperGuard.generated.h"

/**
 *
 */
UCLASS()
class SHADOWSITE_API APaperGuard : public APaperCharacter
{
    GENERATED_BODY()

    /** Camera boom positioning the camera behind the character */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UPawnSensingComponent* SensingComponent;
    class UPawnNoiseEmitterComponent* NoiseEmitterComponent;

    float LastSeenTime;
    float LastHeardTime;

    UPROPERTY(EditDefaultsOnly, Category = "AI")
    float SenseTimeOut;

    bool bSensedTarget;

public:
    APaperGuard();

    // Called every frame
    virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditDefaultsOnly, Category = "AI")
    class UBehaviorTree* BehaviorTree;

    UPROPERTY(EditAnywhere, Category = "AI")
    uint8 BehaviorType;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    UFUNCTION()
    void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                        AActor* OtherActor,
                        UPrimitiveComponent* OtherComp,
                        int32 OtherBodyIndex,
                        bool bFromSweep,
                        const FHitResult &SweepResult);

    UFUNCTION()
    void OnEndOverlap(UPrimitiveComponent* OverlappedComponent,
                            AActor* OtherActor,
                            UPrimitiveComponent* OtherComp,
                            int32 OtherBodyIndex);

    UFUNCTION()
    void OnSeePlayer(APawn* Pawn);

    UFUNCTION()
    void OnHearNoise(APawn* NoisyPawn, const FVector& Location, float Volume);

    UFUNCTION()
    void OnTrapTimerExpired();

    UFUNCTION()
    void OnSnareTimerExpired();

private:
    class UCharacterMovementComponent* MovementComponent;

    float NormalWalkSpeed = 250.0f;
    float SlowedWalkSpeed = 50.0f;
    float TrapSlowPeriod = 5.0f;
    float TrapSnarePeriod = 8.0f;
    float SnareBufferPeriod = 2.0f;
    bool bIsTrapped = false;
    bool bCanBeSnared = true;

    FTimerHandle TrapTimerHandle;
    FTimerHandle NextSnareTimerHandle;

};
