// Fill out your copyright notice in the Description page of Project Settings.


#include "PaperCharacterMovementComponent.h"

#include "UnrealNetwork.h"

void UPaperCharacterMovementComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UPaperCharacterMovementComponent, PlayerHeading);
}

EHeading UPaperCharacterMovementComponent::GetPlayerHeadingEnum() const
{
    return PlayerHeading;
}

void UPaperCharacterMovementComponent::OnMovementModeChanged(EMovementMode PreviousMovementMode,
                                                             uint8 PreviousCustomMode)
{
    Super::OnMovementModeChanged(PreviousMovementMode, PreviousCustomMode);

}

void UPaperCharacterMovementComponent::PhysCustom(float deltaTime, int32 Iterations)
{
    Super::PhysCustom(deltaTime, Iterations);

    switch(CustomMovementMode)
    {
        case static_cast<int>(ECustomMovementType::Climb):
            PhysCustomClimb(deltaTime, Iterations);
            break;

        default:
            break;
    }
}

void UPaperCharacterMovementComponent::PhysCustomClimb(float deltaTime, int32 Iterations)
{
    if (deltaTime < MIN_TICK_TIME)
    {
        return;
    }

    RestorePreAdditiveRootMotionVelocity();

    if( !HasAnimRootMotion() && !CurrentRootMotion.HasOverrideVelocity() )
    {
        if( bCheatFlying && Acceleration.IsZero() )
        {
            Velocity = FVector::ZeroVector;
        }
        const float Friction = GroundFriction;//0.5f * GetPhysicsVolume()->FluidFriction;
        CalcVelocity(deltaTime, Friction, true, GetMaxBrakingDeceleration());
    }

    ApplyRootMotionToVelocity(deltaTime);

    Iterations++;
    bJustTeleported = false;

    FVector OldLocation = UpdatedComponent->GetComponentLocation();
    const FVector Adjusted = Velocity * deltaTime;
    FHitResult Hit(1.f);
    SafeMoveUpdatedComponent(Adjusted, UpdatedComponent->GetComponentQuat(), true, Hit);

    if (Hit.Time < 1.f)
    {
        const FVector GravDir = FVector(0.f, 0.f, -1.f);
        const FVector VelDir = Velocity.GetSafeNormal();
        const float UpDown = GravDir | VelDir;

        bool bSteppedUp = false;
        if ((FMath::Abs(Hit.ImpactNormal.Z) < 0.2f) && (UpDown < 0.5f) && (UpDown > -0.2f) && CanStepUp(Hit))
        {
            float stepZ = UpdatedComponent->GetComponentLocation().Z;
            bSteppedUp = StepUp(GravDir, Adjusted * (1.f - Hit.Time), Hit);
            if (bSteppedUp)
            {
                OldLocation.Z = UpdatedComponent->GetComponentLocation().Z + (OldLocation.Z - stepZ);
            }
        }

        if (!bSteppedUp)
        {
            //adjust and try again
            HandleImpact(Hit, deltaTime, Adjusted);
            SlideAlongSurface(Adjusted, (1.f - Hit.Time), Hit.Normal, Hit, true);
        }
    }

    //if(CurrentFloor.IsWalkableFloor() && CurrentFloor.HitResult.bStartPenetrating)
    //{
    //    SetMovementMode(MOVE_Walking);
    //}

    if( !bJustTeleported && !HasAnimRootMotion() && !CurrentRootMotion.HasOverrideVelocity() )
    {
        Velocity = (UpdatedComponent->GetComponentLocation() - OldLocation) / deltaTime;
    }
}

void UPaperCharacterMovementComponent::TickComponent(float DeltaTime,
                                                     enum ELevelTick TickType,
                                                     FActorComponentTickFunction *ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    // TODO : Only server can update heading???
    const FVector VelDir = Velocity.GetSafeNormal();

    if(Velocity.Size() > 0.0f)
    {
        float HeadingAngle = FMath::Clamp(FMath::Atan(FMath::Abs(VelDir.X / VelDir.Y)), 0.0f, PI / 2.0f);

        if(HeadingAngle < PI / 4.0f)
        {
            if(VelDir.Y > 0.0f)
            {
                PlayerHeading = EHeading::Right;
            }
            else if(VelDir.Y < 0.0f)
            {
                PlayerHeading = EHeading::Left;
            }
        }
        else
        {
            if(VelDir.X > 0.0f)
            {
                PlayerHeading = EHeading::Forward;
            }
            else if(VelDir.X < 0.0f)
            {
                PlayerHeading = EHeading::Backward;
            }
        }

        if(VelDir.Z > 0.0f)
        {
            PlayerHeading = EHeading::Up;
        }
        else if(VelDir.Z < 0.0f)
        {
            PlayerHeading = EHeading::Down;
        }
    }
}

/*
  if (CharacterOwner->Role > ROLE_SimulatedProxy)
    {
        SCOPE_CYCLE_COUNTER(STAT_CharacterMovementNonSimulated);

        // If we are a client we might have received an update from the server.
        const bool bIsClient = (CharacterOwner->Role == ROLE_AutonomousProxy && IsNetMode(NM_Client));
        if (bIsClient)
        {
            ClientUpdatePositionAfterServerUpdate();
        }

        // Allow root motion to move characters that have no controller.
        if( CharacterOwner->IsLocallyControlled() || (!CharacterOwner->Controller && bRunPhysicsWithNoController) || (!CharacterOwner->Controller && CharacterOwner->IsPlayingRootMotion()) )
        {
            {
                SCOPE_CYCLE_COUNTER(STAT_CharUpdateAcceleration);

                // We need to check the jump state before adjusting input acceleration, to minimize latency
                // and to make sure acceleration respects our potentially new falling state.
                CharacterOwner->CheckJumpInput(DeltaTime);

                // apply input to acceleration
                Acceleration = ScaleInputAcceleration(ConstrainInputAcceleration(InputVector));
                AnalogInputModifier = ComputeAnalogInputModifier();
            }

            if (CharacterOwner->Role == ROLE_Authority)
            {
                PerformMovement(DeltaTime);
            }
            else if (bIsClient)
            {
                ReplicateMoveToServer(DeltaTime, Acceleration);
            }
        }
        else if (CharacterOwner->GetRemoteRole() == ROLE_AutonomousProxy)
        {
            // Server ticking for remote client.
            // Between net updates from the client we need to update position if based on another object,
            // otherwise the object will move on intermediate frames and we won't follow it.
            MaybeUpdateBasedMovement(DeltaTime);
            MaybeSaveBaseLocation();

            // Smooth on listen server for local view of remote clients. We may receive updates at a rate different than our own tick rate.
            if (CharacterMovementCVars::NetEnableListenServerSmoothing && !bNetworkSmoothingComplete && IsNetMode(NM_ListenServer))
            {
                SmoothClientPosition(DeltaTime);
            }
        }
    }
    else if (CharacterOwner->Role == ROLE_SimulatedProxy)
    {
        if (bShrinkProxyCapsule)
        {
            AdjustProxyCapsuleSize();
        }
        SimulatedTick(DeltaTime);
    }
*/
