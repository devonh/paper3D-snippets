// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PaperCharacterMovementComponent.generated.h"

UENUM(BlueprintType)
enum class ECustomMovementType : uint8
{
    Climb UMETA(DisplayName="Climb")
};

UENUM(BlueprintType)
enum class EHeading : uint8
{
    Forward  UMETA(DisplayName="Forward"),
    Backward UMETA(DisplayName="Backward"),
    Right    UMETA(DisplayName="Right"),
    Left     UMETA(DisplayName="Left"),
    Up       UMETA(DisplayName="Up"),
    Down     UMETA(DisplayName="Down")
};

/**
 *
 */
UCLASS()
class SHADOWSITE_API UPaperCharacterMovementComponent : public UCharacterMovementComponent
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category="Heading")
    EHeading GetPlayerHeadingEnum() const;

protected:
    virtual void OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode) override;
    virtual void PhysCustom(float deltaTime, int32 Iterations) override;

    virtual void TickComponent(float DeltaTime,
                               enum ELevelTick TickType,
                               FActorComponentTickFunction *ThisTickFunction) override;

    void PhysCustomClimb(float deltaTime, int32 Iterations);

    UPROPERTY(Replicated)
    EHeading PlayerHeading;

};
