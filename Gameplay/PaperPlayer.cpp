// Fill out your copyright notice in the Description page of Project Settings.


#include "PaperPlayer.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/PawnNoiseEmitterComponent.h"
#include "Ladder.h"
#include "PaperPlayerOutline.h"
#include "DeployableTrap.h"
#include "PaperSword.h"
#include "PaperCharacterMovementComponent.h"
#include "PaperFlipbookComponent.h"
#include "PaperFlipbook.h"

#include "UnrealNetwork.h"

#include "DrawDebugHelpers.h"

APaperPlayer::APaperPlayer(const class FObjectInitializer& ObjectInitializer)
        : Super(ObjectInitializer.SetDefaultSubobjectClass<UPaperCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
    PrimaryActorTick.bCanEverTick = true;

    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character
    CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller
    CameraBoom->bDoCollisionTest = false;

    FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
    FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

    // Setup some better character movement default values
    UPaperCharacterMovementComponent* CharacterMovementComponent =
        Cast<UPaperCharacterMovementComponent>(GetCharacterMovement());
    check(CharacterMovementComponent != nullptr);
    MovementComponent = CharacterMovementComponent;
    MovementComponent->MaxWalkSpeed = 600.0f;
    MovementComponent->GravityScale = 4.0f;

    bUseControllerRotationPitch = 0;
    bUseControllerRotationYaw = 0;
    bUseControllerRotationRoll = 0;

    NoiseEmitterComponent = CreateDefaultSubobject<UPawnNoiseEmitterComponent>(TEXT("NoiseEmitterComponent"));

    // NOTE : The Flipbook is currently set in the derived blueprint
}

void APaperPlayer::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(APaperPlayer, bIsHoldingTrap);
}

void APaperPlayer::BeginPlay()
{
    Super::BeginPlay();

    // NOTE : These don't work if done in the constructor.
    // Alternative would be to use OnActorBeginOverlap(AActor* Self, AActor* OtherActor)
    GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &APaperPlayer::OnBeginOverlap);
    GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &APaperPlayer::OnEndOverlap);

    GetWorld()->GetTimerManager().SetTimer(NoiseTimerHandle, this, &APaperPlayer::OnMakeNoise, MakeNoiseRate, true);

    SERVER_SpawnSword();
}

void APaperPlayer::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    UpdateAnimation();

#if !UE_BUILD_SHIPPING
    // DEBUG : Enable this for player velocity debugging
    FVector ActorLocation = GetActorLocation();

    bool bPersistentShowTrace = false;
    float TraceLifetime = -1.0f;
    uint8 DepthPriority = 0;
    float TraceThickness = 2.0f;
    DrawDebugLine(GetWorld(), ActorLocation, ActorLocation + CurrentVelocity * 200, FColor::Red, bPersistentShowTrace, TraceLifetime, DepthPriority, TraceThickness);
    DrawDebugLine(GetWorld(), ActorLocation, ActorLocation + GetActorForwardVector() * 200, FColor::Green);

    //UE_LOG(LogTemp, Warning, TEXT("Player Heading: %i"), static_cast<int>(MovementComponent->GetPlayerHeadingEnum()));
#endif
}

void APaperPlayer::UpdateAnimation()
{
    GetSprite()->SetFlipbook(AnimationFlipbooks[static_cast<int>(GetAnimationState())]);
}

EAnimationState APaperPlayer::GetAnimationState() const
{
    float CurrentSpeed = GetVelocity().Size();
    EHeading CurrentHeading = MovementComponent->GetPlayerHeadingEnum();
    EAnimationState AnimationState;

    switch(CurrentHeading)
    {
        case EHeading::Forward:
            AnimationState = EAnimationState::MoveForward;
            break;

        case EHeading::Backward:
            AnimationState = EAnimationState::MoveBackward;
            break;

        case EHeading::Right:
            AnimationState = EAnimationState::MoveRight;
            break;

        case EHeading::Left:
            AnimationState = EAnimationState::MoveLeft;
            break;

        case EHeading::Up:
            AnimationState = EAnimationState::ClimbUp;
            break;

        case EHeading::Down:
            if(bIsOnLadder)
            {
                AnimationState = EAnimationState::ClimbDown;
            }
            else
            {
                AnimationState = EAnimationState::Fall;
            }
            break;
    }

    if(!(CurrentSpeed > 0.0f))
    {
        AnimationState = EAnimationState::Idle;
    }

    return AnimationState;
}

void APaperPlayer::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
    check(PlayerInputComponent);

    PlayerInputComponent->BindAxis("MoveForward", this, &APaperPlayer::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &APaperPlayer::MoveRight);
    PlayerInputComponent->BindAction("Sneak", IE_Pressed, this, &APaperPlayer::StartSneaking);
    PlayerInputComponent->BindAction("Sneak", IE_Released, this, &APaperPlayer::StopSneaking);
    PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APaperPlayer::StartSprinting);
    PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APaperPlayer::StopSprinting);
    PlayerInputComponent->BindAction("DeployTrap", IE_Pressed, this, &APaperPlayer::DeployTrap);
}

FVector APaperPlayer::CalculateVelocityWhileOnLadder(FVector2D PlayerVelocityXY) const
{
    FVector VelocityOnLadder = FVector();
    FVector2D PlayerPositionXY = FVector2D(GetActorLocation().X, GetActorLocation().Y);
    FVector2D LadderFaceXY = FVector2D(CurrentLadder.LadderFaceDirection.X, CurrentLadder.LadderFaceDirection.Y);
    FVector2D LadderPositionXY = FVector2D(CurrentLadder.LadderWorldLocation.X, CurrentLadder.LadderWorldLocation.Y);

    FFindFloorResult FloorResult;
    MovementComponent->FindFloor(GetActorLocation(), FloorResult, true);

    UE_LOG(LogTemp, Warning, TEXT("Floor Distance: %f"),  FloorResult.GetDistanceToFloor());

    if(FloorResult.IsWalkableFloor() && FloorResult.GetDistanceToFloor() < 10.0f)
    {
        // NOTE : Apply the regular XY velocity so the player can move away from the ladder while on the floor.
        VelocityOnLadder.X = PlayerVelocityXY.X;
        VelocityOnLadder.Y = PlayerVelocityXY.Y;

        // NOTE : Apply the portion of player velocity in the opposite direction of the ladder's
        // face to the player's z-velocity.
        VelocityOnLadder.Z = FMath::Clamp(-FVector2D::DotProduct(PlayerVelocityXY.GetSafeNormal(),
                                                                 LadderFaceXY.GetSafeNormal()),
                                          0.0f, 1.0f); // No use going down when we are at the bottom.
    }
    else
    {
        // NOTE : Project player velocity onto the plane orthogonal to ladder face direction.
        // This stops the player from moving away from the ladder when going down.
        FVector2D NewVelocity = PlayerVelocityXY - FVector2D::DotProduct(PlayerVelocityXY, LadderFaceXY) * LadderFaceXY;
        VelocityOnLadder.X = NewVelocity.X;
        VelocityOnLadder.Y = NewVelocity.Y;

        // NOTE : Apply the portion of player velocity in the opposite direction of the ladder's
        // face to the player's z-velocity.
        VelocityOnLadder.Z = -FVector2D::DotProduct(PlayerVelocityXY.GetSafeNormal(),
                                                    LadderFaceXY.GetSafeNormal());
    }

    return VelocityOnLadder.GetSafeNormal();
}

void APaperPlayer::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                  AActor* OtherActor,
                                  UPrimitiveComponent* OtherComp,
                                  int32 OtherBodyIndex,
                                  bool bFromSweep,
                                  const FHitResult &SweepResult)
{
     if(Cast<ALadder>(OtherActor))
    {
        bIsOnLadder = true;
        MovementComponent->SetMovementMode(MOVE_Custom, static_cast<int>(ECustomMovementType::Climb));
        MovementComponent->StopMovementImmediately();

        CurrentLadder.LadderWorldLocation = OtherActor->GetActorLocation();
        CurrentLadder.LadderFaceDirection = OtherActor->GetActorForwardVector();

        // NOTE : Turn off gravity while on the ladder.
        PreviousGravityScale = MovementComponent->GravityScale;
        MovementComponent->GravityScale = 0.0f;

        // NOTE : Maintain full control in the "air" while on ladder.
        PreviousAirControl = MovementComponent->AirControl;
        MovementComponent->AirControl = 1.0f;
    }
}

void APaperPlayer::OnEndOverlap(UPrimitiveComponent* OverlappedComponent,
                                AActor* OtherActor,
                                UPrimitiveComponent* OtherComp,
                                int32 OtherBodyIndex)
{
    if(Cast<ALadder>(OtherActor))
    {
        bIsOnLadder = false;
        MovementComponent->SetMovementMode(MOVE_Falling);

        // NOTE : Reset previous gravity and air control values.
        MovementComponent->GravityScale = PreviousGravityScale;
        PreviousAirControl = MovementComponent->AirControl;

        // NOTE : Clear Z Velocity or it can linger after stepping off a ladder.
        CurrentVelocity.Z = 0;
    }
}

void APaperPlayer::OnMakeNoise()
{
    NoiseEmitterComponent->MakeNoise(this, 1.0, GetActorLocation());

    FActorSpawnParameters SpawnParams;
    SpawnParams.Owner = this;
    SpawnParams.Instigator = Instigator;

    if(PlayerOutlineClass)
    {
        // NOTE : Step the location toward the camera to prevent fighting for the same pixel location.
        // TODO : The outline sprite rotation is incorrect relative to the player.
        if(bShowPlayerOutline)
        {
            APaperPlayerOutline* PlayerOutline =
                GetWorld()->SpawnActor<APaperPlayerOutline>(PlayerOutlineClass,
                                                            GetActorLocation() + FVector(-0.01, 0, 0),
                                                            GetActorRotation().Add(0, 90, 0),
                                                            SpawnParams);
        }
    }
}

bool APaperPlayer::SERVER_SpawnSword_Validate()
{
    return true;
}

void APaperPlayer::SERVER_SpawnSword_Implementation()
{
    FActorSpawnParameters SpawnParams;
    SpawnParams.Owner = this;
    SpawnParams.Instigator = Instigator;

    Sword = GetWorld()->SpawnActor<APaperSword>(SwordClass,
                                                GetActorLocation() + FVector(-0.1, 0, 0),
                                                FRotator(0, 0, 0),
                                                SpawnParams);

    if(Sword)
    {
        Sword->AttachToComponent((USceneComponent*)GetSprite(),
                                 FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true),
                                 FName("LeftHand"));
    }
}

bool APaperPlayer::SERVER_SpawnTrap_Validate(FVector SpawnLocation)
{
    return true;
}

void APaperPlayer::SERVER_SpawnTrap_Implementation(FVector SpawnLocation)
{
    FActorSpawnParameters SpawnParams;
    SpawnParams.Owner = this;
    SpawnParams.Instigator = Instigator;

    DeployableTrap = GetWorld()->SpawnActor<ADeployableTrap>(SpawnLocation,
                                                             FRotator(0, 0, 0),
                                                             SpawnParams);

    if(DeployableTrap)
    {
        DeployableTrap->AttachToComponent((USceneComponent*)GetSprite(),
                                          FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true),
                                          FName("DeployLocation"));
    }
}

bool APaperPlayer::SERVER_DeployTrap_Validate()
{
    return true;
}

void APaperPlayer::SERVER_DeployTrap_Implementation()
{
    if(DeployableTrap)
    {
        DeployableTrap->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, false));
        DeployableTrap = nullptr;
    }
}

float APaperPlayer::GetCurrentSpeedScale() const
{
    float SpeedScale = 0.0f;

    if(!CurrentVelocity.IsZero())
    {
        if(bIsSneaking)
        {
            // NOTE : Sneaking will override the sprinting input
            SpeedScale = SneakSpeedScale;
        }
        else if(bIsSprinting)
        {
            SpeedScale = SprintSpeedScale;
        }
        else
        {
            SpeedScale = BaseSpeedScale;
        }
    }

#if !UE_BUILD_SHIPPING
    // DEBUG : Enable this to see current player speed in the logs
    //UE_LOG(LogTemp, Warning, TEXT("Player Speed: %f"), SpeedScale * MovementComponent->MaxWalkSpeed);
#endif

    return SpeedScale;
}

void APaperPlayer::MovePlayerOnLadder()
{
    CurrentVelocity.Normalize();

    if(CurrentVelocity.Size() > 0.0f)
    {
        // NOTE : Each tick reset the acceleration for the impulse movement to feel correct.
        //MovementComponent->StopMovementImmediately();

        CurrentVelocity = CalculateVelocityWhileOnLadder(FVector2D(CurrentVelocity.X, CurrentVelocity.Y));
        // NOTE : Velocity is severely dampened when falling and vertical movement isn't applied using
        // AddMovementInput. Use AddImpulse instead to move the player.
        //MovementComponent->AddImpulse(CurrentVelocity * GetCurrentSpeedScale() * 30000);

        //MovementComponent->AddInputVector(CurrentVelocity);

        AddMovementInput(CurrentVelocity, GetCurrentSpeedScale());
    }
}

void APaperPlayer::MoveForward(float Value)
{
    CurrentVelocity.X = FMath::Clamp(Value, -1.0f, 1.0f);

    if((Controller != NULL) && (Value != 0.0f))
    {
        if(bIsOnLadder)
        {
            MovePlayerOnLadder();
        }
        else
        {
            // Get forward vector
            const FRotator Rotation = Controller->GetControlRotation();
            const FRotator YawRotation(0, Rotation.Yaw, 0);
            const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

            AddMovementInput(Direction, Value * GetCurrentSpeedScale());
        }
    }
}

void APaperPlayer::MoveRight(float Value)
{
    CurrentVelocity.Y = FMath::Clamp(Value, -1.0f, 1.0f);

    if((Controller != NULL) && (Value != 0.0f))
    {
        if(bIsOnLadder)
        {
            MovePlayerOnLadder();
        }
        else
        {
            // Get right vector
            const FRotator Rotation = Controller->GetControlRotation();
            const FRotator YawRotation(0, Rotation.Yaw, 0);
            const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

            AddMovementInput(Direction, Value * GetCurrentSpeedScale());
        }
    }
}

void APaperPlayer::StartSneaking()
{
    bIsSneaking = true;
}

void APaperPlayer::StopSneaking()
{
    bIsSneaking = false;
}

void APaperPlayer::StartSprinting()
{
    bIsSprinting = true;
}

void APaperPlayer::StopSprinting()
{
    bIsSprinting = false;
}

void APaperPlayer::DeployTrap()
{
    if(bIsHoldingTrap)
    {
        // NOTE : Try and place the trap on the ground.
        SERVER_DeployTrap();
        bIsHoldingTrap = false;
    }
    else
    {
        // NOTE : Add a trap to the player's hand.
        SERVER_SpawnTrap(GetActorLocation() + FVector(0, 100, 0));
        bIsHoldingTrap = true;
    }
}
