// Fill out your copyright notice in the Description page of Project Settings.


#include "Ladder.h"
#include "Components/BoxComponent.h"

// Sets default values
ALadder::ALadder()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    //PrimaryActorTick.bCanEverTick = true;

    LadderCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("RootObject"));
    RootComponent = LadderCollision;
    FVector BoxSize = FVector(2.5f, 25.0f, 70.0f);
    LadderCollision->SetBoxExtent(BoxSize);
    LadderCollision->SetCollisionProfileName(TEXT("Ladder"));

    LadderVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
    LadderVisual->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<UStaticMesh> LadderVisualAsset(TEXT("/Game/ThirdPersonCPP/Assets/Objects/Ladder/Ladder_Mesh.Ladder_Mesh"));
    if(LadderVisualAsset.Succeeded())
    {
        LadderVisual->SetStaticMesh(LadderVisualAsset.Object);
        LadderVisual->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));

        float MeshSize = 50.0f; // NOTE : Mesh is larger than collision so scale it down.
        LadderVisual->SetWorldScale3D(FVector(BoxSize.X / MeshSize,
                                              BoxSize.Y / MeshSize,
                                              BoxSize.Z / MeshSize));
    }
}

// Called when the game starts or when spawned
void ALadder::BeginPlay()
{
    Super::BeginPlay();

}

// Called every frame
void ALadder::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}
