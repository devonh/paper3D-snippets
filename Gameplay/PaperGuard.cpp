// Fill out your copyright notice in the Description page of Project Settings.


#include "PaperGuard.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Perception/PawnSensingComponent.h"
#include "Components/PawnNoiseEmitterComponent.h"
#include "Components/CapsuleComponent.h"
#include "GuardAIController.h"
#include "DrawDebugHelpers.h"
#include "DeployableTrap.h"

APaperGuard::APaperGuard()
{
    UCharacterMovementComponent* CharacterMovementComponent = GetCharacterMovement();
    check(CharacterMovementComponent != nullptr);
    MovementComponent = CharacterMovementComponent;
    MovementComponent->MaxWalkSpeed = NormalWalkSpeed;
    MovementComponent->GravityScale = 4.0f;

    SensingComponent = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("SensingComponent"));
    SensingComponent->HearingThreshold = 500;
    SensingComponent->LOSHearingThreshold = 1000;
    SensingComponent->SightRadius = 750;
    SensingComponent->SetPeripheralVisionAngle(30.0f);
    NoiseEmitterComponent = CreateDefaultSubobject<UPawnNoiseEmitterComponent>(TEXT("NoiseEmitterComponent"));

    bUseControllerRotationPitch = 0;
    bUseControllerRotationYaw = 0;
    bUseControllerRotationRoll = 0;

    // NOTE : AIController class is set in blueprint
    //AIControllerClass = AGuardAIController::StaticClass();
    BehaviorType = 0;
    SenseTimeOut = 3.0f;
}

void APaperGuard::BeginPlay()
{
    Super::BeginPlay();

    GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &APaperGuard::OnBeginOverlap);
    GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &APaperGuard::OnEndOverlap);
    SensingComponent->OnSeePawn.AddDynamic(this, &APaperGuard::OnSeePlayer);
    SensingComponent->OnHearNoise.AddDynamic(this, &APaperGuard::OnHearNoise);
}

void APaperGuard::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    bool bVisionTimeout = (GetWorld()->TimeSeconds - LastSeenTime) > SenseTimeOut;
    bool bHearingTimeout = (GetWorld()->TimeSeconds - LastHeardTime) > SenseTimeOut;
    if(bSensedTarget && bVisionTimeout && bHearingTimeout)
    {
        AGuardAIController* AIController = Cast<AGuardAIController>(GetController());
        if(AIController)
        {
            bSensedTarget = false;
            AIController->SetTargetEnemy(nullptr);
        }
    }
}

void APaperGuard::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                 AActor* OtherActor,
                                 UPrimitiveComponent* OtherComp,
                                 int32 OtherBodyIndex,
                                 bool bFromSweep,
                                 const FHitResult &SweepResult)
{
    ADeployableTrap* DeployableTrap;
    if((DeployableTrap = Cast<ADeployableTrap>(OtherActor)))
    {
        switch(DeployableTrap->TrapType)
        {
            case ETrapType::SLOW:
                UE_LOG(LogTemp, Warning, TEXT("Guard is overlapping with SLOW trap type."));
                bIsTrapped = true;
                MovementComponent->MaxWalkSpeed = SlowedWalkSpeed;
                break;

            case ETrapType::SNARE:
                if(bCanBeSnared)
                {
                    UE_LOG(LogTemp, Warning, TEXT("Guard is overlapping with SNARE trap type."));
                    bIsTrapped = true;
                    MovementComponent->MaxWalkSpeed = 0.0f;

                    bCanBeSnared = false;
                    GetWorld()->GetTimerManager().SetTimer(TrapTimerHandle,
                                                           this,
                                                           &APaperGuard::OnTrapTimerExpired,
                                                           TrapSnarePeriod, false);
                }
                break;

            default:
                UE_LOG(LogTemp, Warning, TEXT("Guard is overlapping with UNKNOWN trap type."));
                break;
        }
    }
}

void APaperGuard::OnEndOverlap(UPrimitiveComponent* OverlappedComponent,
                               AActor* OtherActor,
                               UPrimitiveComponent* OtherComp,
                               int32 OtherBodyIndex)
{
    ADeployableTrap* DeployableTrap;
    if((DeployableTrap = Cast<ADeployableTrap>(OtherActor)))
    {
        switch(DeployableTrap->TrapType)
        {
            case ETrapType::SLOW:
                GetWorld()->GetTimerManager().SetTimer(TrapTimerHandle,
                                                       this,
                                                       &APaperGuard::OnTrapTimerExpired,
                                                       TrapSlowPeriod,
                                                       false);
                break;

            case ETrapType::SNARE:
                break;

            default:
                UE_LOG(LogTemp, Warning, TEXT("Guard stopped overlapping with UNKNOWN trap type."));
                break;
        }
    }
}

void APaperGuard::OnTrapTimerExpired()
{
    bIsTrapped = false;
    MovementComponent->MaxWalkSpeed = NormalWalkSpeed;
    GetWorld()->GetTimerManager().SetTimer(NextSnareTimerHandle,
                                           this,
                                           &APaperGuard::OnSnareTimerExpired,
                                           SnareBufferPeriod,
                                           false);
}

void APaperGuard::OnSnareTimerExpired()
{
    bCanBeSnared = true;
}

void APaperGuard::OnSeePlayer(APawn* Pawn)
{
    LastSeenTime = GetWorld()->TimeSeconds;
    bSensedTarget = true;

    AGuardAIController* AIController = Cast<AGuardAIController>(GetController());
    APaperCharacter* SensedPawn = Cast<APaperCharacter>(Pawn);
    if(AIController && SensedPawn)
    {
        AIController->SetMoveToTarget(SensedPawn);
    }
}

void APaperGuard::OnHearNoise(APawn* NoisyPawn, const FVector& Location, float Volume)
{
    LastHeardTime = GetWorld()->TimeSeconds;
    bSensedTarget = true;

    DrawDebugDirectionalArrow(GetWorld(), GetActorLocation(), Location, 64, FColor::Blue, false, 0.2f);

    AGuardAIController* AIController = Cast<AGuardAIController>(GetController());
    if(AIController)
    {
        AIController->SetMoveToTarget(NoisyPawn);
    }
}
