// Fill out your copyright notice in the Description page of Project Settings.


#include "DeployableTrap.h"
#include "Components/BoxComponent.h"

// Sets default values
ADeployableTrap::ADeployableTrap()
{
    bReplicates = true;

    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    //PrimaryActorTick.bCanEverTick = true;

    TrapCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("RootObject"));
    RootComponent = TrapCollision;
    FVector BoxSize = FVector(45.0f, 45.0f, 10.0f);
    TrapCollision->SetBoxExtent(BoxSize);
    TrapCollision->SetCollisionProfileName(TEXT("DeployableTrap"));

    TrapVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
    TrapVisual->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<UStaticMesh> TrapVisualAsset(TEXT("/Game/ThirdPersonCPP/Assets/Objects/Ladder/Ladder_Mesh.Ladder_Mesh"));
    if(TrapVisualAsset.Succeeded())
    {
        TrapVisual->SetStaticMesh(TrapVisualAsset.Object);
        TrapVisual->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));

        float MeshSize = 50.0f;
        TrapVisual->SetWorldScale3D(FVector(BoxSize.X / MeshSize,
                                            BoxSize.Y / MeshSize,
                                            BoxSize.Z / MeshSize));
    }
}

// Called when the game starts or when spawned
void ADeployableTrap::BeginPlay()
{
    Super::BeginPlay();

}

// Called every frame
void ADeployableTrap::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}
