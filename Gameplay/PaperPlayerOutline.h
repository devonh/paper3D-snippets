// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperSpriteActor.h"
#include "PaperPlayerOutline.generated.h"

/**
 *
 */
UCLASS()
class SHADOWSITE_API APaperPlayerOutline : public APaperSpriteActor
{
    GENERATED_BODY()

    APaperPlayerOutline();

    virtual void BeginPlay() override;

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
        float LifetimeSeconds = 3.0f;

protected:
    UFUNCTION()
        void OnDestroyTimeout();

private:
    FTimerHandle DestroyTimerHandle;

};
