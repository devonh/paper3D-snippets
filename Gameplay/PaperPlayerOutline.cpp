// Fill out your copyright notice in the Description page of Project Settings.


#include "PaperPlayerOutline.h"

APaperPlayerOutline::APaperPlayerOutline()
{

}

void APaperPlayerOutline::BeginPlay()
{
    Super::BeginPlay();

    GetWorld()->GetTimerManager().SetTimer(DestroyTimerHandle, this,
                                           &APaperPlayerOutline::OnDestroyTimeout, LifetimeSeconds, false);
}

void APaperPlayerOutline::OnDestroyTimeout()
{
    Destroy();
}
