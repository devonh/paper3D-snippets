// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "PaperPlayer.generated.h"

UENUM(BlueprintType)
enum class EAnimationState : uint8
{
    Idle         UMETA(DisplayName="Idle"),
    MoveForward  UMETA(DisplayName="Forward"),
    MoveBackward UMETA(DisplayName="Backward"),
    MoveRight    UMETA(DisplayName="Right"),
    MoveLeft     UMETA(DisplayName="Left"),
    ClimbUp      UMETA(DisplayName="Up"),
    ClimbDown    UMETA(DisplayName="Down"),
    Fall         UMETA(DisplayName="Fall"),
};

USTRUCT()
struct FLadderProperties
{
    GENERATED_BODY()

    UPROPERTY()
    FVector LadderWorldLocation = FVector();

    UPROPERTY()
    FVector LadderFaceDirection = FVector();
};

UCLASS()
class SHADOWSITE_API APaperPlayer : public APaperCharacter
{
    GENERATED_BODY()

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* CameraBoom;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* FollowCamera;

    class UPawnNoiseEmitterComponent* NoiseEmitterComponent;

public:
    APaperPlayer(const class FObjectInitializer& ObjectInitializer);

    // Called every frame
    virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintCallable, Category = Animation)
    void UpdateAnimation();

    // Speed Scales represent the percentage of MaxWalkSpeed for the given movement state
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    float BaseSpeedScale = 0.6f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    float SneakSpeedScale = 0.3f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    float SprintSpeedScale = 1.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    float MakeNoiseRate = 3.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Visuals)
    bool bShowPlayerOutline = true;

    UPROPERTY(EditDefaultsOnly, Category = Outline)
    TSubclassOf<class APaperPlayerOutline> PlayerOutlineClass;

    UPROPERTY(EditDefaultsOnly, Category = Weapon)
    TSubclassOf<class APaperSword> SwordClass;

    UPROPERTY(EditDefaultsOnly, Category = Animation)
    TArray<class UPaperFlipbook*> AnimationFlipbooks;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    // Called to bind functionality to input
    void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    UFUNCTION()
    void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                              AActor* OtherActor,
                              UPrimitiveComponent* OtherComp,
                              int32 OtherBodyIndex,
                              bool bFromSweep,
                              const FHitResult &SweepResult);

    UFUNCTION()
    void OnEndOverlap(UPrimitiveComponent* OverlappedComponent,
                            AActor* OtherActor,
                            UPrimitiveComponent* OtherComp,
                            int32 OtherBodyIndex);

    UFUNCTION()
    void OnMakeNoise();

    UFUNCTION(reliable, server, WithValidation)
    void SERVER_SpawnTrap(FVector SpawnLocation);

    UFUNCTION(reliable, server, WithValidation)
    void SERVER_DeployTrap();

    UFUNCTION(reliable, server, WithValidation)
    void SERVER_SpawnSword();

    float GetCurrentSpeedScale() const;

    void MovePlayerOnLadder();
    void MoveForward(float Value);
    void MoveRight(float Value);

    void StartSneaking();
    void StopSneaking();
    void StartSprinting();
    void StopSprinting();

    void DeployTrap();

private:
    FVector CalculateVelocityWhileOnLadder(FVector2D PlayerVelocityXY) const;
    EAnimationState GetAnimationState() const;

    class UPaperCharacterMovementComponent* MovementComponent;
    class ADeployableTrap* DeployableTrap;
    class APaperSword* Sword;

    FVector CurrentVelocity;
    FLadderProperties CurrentLadder;

    bool bIsSneaking = false;
    bool bIsSprinting = false;
    bool bIsOnLadder = false;

    UPROPERTY(Replicated)
    bool bIsHoldingTrap = false;

    float PreviousGravityScale = 0.0f;
    float PreviousAirControl = 0.0f;

    FTimerHandle NoiseTimerHandle;
};
